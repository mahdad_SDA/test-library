package com.sda.tutorials.demo.service.impl;

import com.sda.tutorials.demo.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

    @Override
    public String logIn() {
        return "User logged in succesfully";
    }
}
