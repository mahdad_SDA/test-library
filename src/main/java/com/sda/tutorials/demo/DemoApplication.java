package com.sda.tutorials.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {

        // load the spring configuration file
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("springContext.xml");
        // retrieve bean from spring container
        Book book = context.getBean("book", Book.class);//refers to @Component("thatSillyCoach") on TennisCoach

        Library library = context.getBean("library", Library.class);
        library.setAddress("Tallin");
        library.setName("Test");
        book.setId(1);
        book.setName("Effective Java");
        book.setPublishYear(1990);
        List<Book>  books = new ArrayList<>();
        books.add(book);
        library.setBookList(books);
        // call methods on the bean
        // System.out.println(book.toString());
        System.out.println(library.toString());
        System.out.println(library.logIn());
        // close the context
        context.close();
	}

}
